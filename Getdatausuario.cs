using System.Threading;
using System.Threading.Tasks;
using Lime.Messaging.Contents;
using Lime.Protocol;
using Take.Blip.Client;

namespace MessageTypes
{
    public class Getdatausuario : IMessageReceiver
    {
        private readonly ISender _sender;

        public Getdatausuario(ISender sender)
        {
            _sender = sender;
        }



        public async Task ReceiveAsync(Message message, CancellationToken cancellationToken)
        {
            var document = new PlainText
            {
                Text = "Por favor informe o primeiro n�mero?"
            };
            await _sender.SendMessageAsync(document, message.From, cancellationToken);
        }

    }
}